// Setup de base
import Vue from 'vue'
import App from './App'

//Importation du framework vue-material + activation du theme par défaut
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

// Importation de vue router
import VueRouter from 'vue-router'

// Importation de vue resource
import VueResource from 'vue-resource'

// Importation de moment
import moment from 'moment'

// Importation des components de l'appli
import Home from './components/Home.vue'
import Search from './components/Search.vue'
import Login from './components/Login.vue'
import Register from './components/Register.vue'
import Logout from './components/Logout.vue'
import WriteArticle from './components/WriteArticle.vue'
import Admin from './components/Admin.vue'
import Profile from './components/Profile.vue'
import DebugBoard from './components/DebugBoard.vue'
import Blog from './components/Blog.vue'
import BlogPost from './components/BlogPost.vue'

import NotFound from './components/NotFound.vue'


// Initialisation
Vue.use(VueMaterial)
Vue.use(VueRouter)
Vue.use(VueResource)
Vue.prototype.moment = moment
Vue.config.productionTip = false

window.Event = new Vue;

const routes = [{
    path: '/',
    component: Home
  },
  {
    path: '/search',
    component: Search
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/register',
    component: Register
  },
  {
    path: '/logout',
    component: Logout
  },
  {
    path: '/write',
    component: WriteArticle
  },
  {
    path: '/admin',
    component: Admin
  },
  {
    path: '/profile',
    component: Profile
  },
  {
    path: '/debugboard',
    component: DebugBoard
  },
  {
    path: '/blog',
    component: Blog
  },
  {
    path: '/blog/:slug',
    component: BlogPost
  },
  {
    path: '*',
    component: NotFound
  }

]

const router = new VueRouter({
  routes,
  mode: 'history'
})

const app = new Vue({
  el: '#app',
  components: {
    App
  },
  template: '<App/>',
  router
}).$mount('#app')
